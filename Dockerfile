FROM node:12
WORKDIR /app

## Development
#CMD npm run dev

## Production
COPY . .
RUN npm i
RUN npm run build
CMD npm start