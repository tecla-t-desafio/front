import portugueseMessages from 'ra-language-portuguese';

const messages = {
    'pt': portugueseMessages,
};
const i18nProvider = {
    getLocale(locale){
        return 'pt';
    }
}

export default i18nProvider;