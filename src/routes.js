import React from "react";
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from "./pages/Home";
import AdminPage from "./pages/Admin";

export default function Routes(props) {
    return (
        <BrowserRouter>
            <Switch>
                <Route path={['/', '/peoples', '/spaceships', '/planets', '/peoples/:id', '/spaceships/:id', '/planets/:id']} exact component={Home}/>
                <Route path="/admin" exact component={AdminPage}/>
            </Switch>
        </BrowserRouter>
    )
}