import React, {useEffect, useState} from "react";
import Card from "../card";
import {
    Container,
    Eye,
    Height,
    BigContainer,
    HeightIcon,
    Info,
    InfoContainer,
    Weight,
    Planet,
    PlanetTitle, PlanetLink, SpaceShips, SpaceShip, Label
} from "./styles";
import peoplesService from "../../services/peoples";
import LoadingSquad from "../loadingSquad";

let initPeople = {
    "name": <LoadingSquad width={150} height={20}/>,
    "height": <LoadingSquad width={80} height={30}/>,
    "mass": <LoadingSquad width={100} height={20}/>,
    "hair_color": <LoadingSquad width={100} height={25}/>,
    "skin_color": <LoadingSquad width={100} height={25}/>,
    "eye_color": <LoadingSquad width={100} height={25}/>,
    "homeworld": "http://swapi.dev/api/planets/1/",
    "spaceShips": [
        {name: <LoadingSquad width={120} height={25}/>, id: '#'}
    ],
};

export default function PeopleInfo(props) {
    let [people, setPeople] = useState(initPeople);

    let {match, seterror} = props;
    let {id} = match.params;

    let {mass, hair_color, skin_color, eye_color, height, homeworld} = people;
    homeworld = homeworld.split('/');
    let planetId = homeworld[homeworld.length - 2];

    useEffect(() => {
        setPeople(initPeople);
        peoplesService.getOne(id, true).then((data) => {
            setPeople(data);
        }).catch(() => setPeople(old =>({...old, name: 'Error'})))
    }, [props.match.params.id])

    return (
        <Card title={people.name} notFilter>
            <Label>
                Informações
            </Label>
            <Container>
                <InfoContainer>
                    <Info><Weight/>{mass} quilos</Info>
                    <Info><Eye/>{eye_color}</Info>
                    <Info>Cor do cabelo: <strong>{hair_color}</strong></Info>
                    <Info>Cor da pele: <strong>{skin_color}</strong></Info>
                </InfoContainer>
                <BigContainer>
                    <HeightIcon/>
                    <Height>{height} cm</Height>
                </BigContainer>
                <BigContainer>
                    <Planet/>
                    <PlanetTitle>Planeta Natal</PlanetTitle>
                    <PlanetLink to={`/planets/${planetId}`}>Visitar</PlanetLink>
                </BigContainer>
            </Container>
            {people.spaceShips.length > 0 && <>
                <Label>
                    Naves espaciais
                </Label>
                <SpaceShips>
                    {people.spaceShips.map(({name, id}, i) => <SpaceShip to={`/spaceships/${id}`}
                                                                         key={i}>{name}</SpaceShip>)}
                </SpaceShips>
            </>}
        </Card>
    );
}