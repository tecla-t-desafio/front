import styled from "styled-components";

export const Container = styled.span`
    padding: 0.6em 1em;
    font-size: 1.2em;
    color: #33333;
    font-weight: normal;
    background: ${props => props.error ? 'red' : 'linear-gradient(to right, #00c6e9, #18db9f)}'};
    color: white;
    text-transform: uppercase;
    border-radius: 12px 0 12px 0;
    margin-right: 0.6em;
    align-self: flex-start;
`;