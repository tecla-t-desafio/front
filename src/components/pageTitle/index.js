import React from "react";
import {Container} from "./styles";

export default function PageTitle(props){
    let {title, error} = props;
    return(
        <Container error={error}>
            {title}
        </Container>
    )
}