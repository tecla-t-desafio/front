import React from "react";
import {Container, Model, Man, Name, Woman} from "./styles";
import translate from "../../addons/translate";
import {Link} from "react-router-dom";

export default function SpaceShip(props) {
    let {name, model, selected, id} = props;

    return (
        <Link to={selected ? '/spaceships' : `/spaceships/${id}`}>
            <Container selected={selected}>
                <Name>{name}</Name>
                <Model>{model}</Model>
            </Container>
        </Link>
    )
}