import React, {useEffect, useState} from "react";
import Card from "../card";
import {
    Container,
    Orbit,
    Height,
    BigContainer,
    Mass,
    Info,
    InfoContainer,
    Rotation,
    Planet,
    PlanetTitle, List, Item, Label, Population
} from "./styles";
import planetsService from "../../services/planets";
import LoadingSquad from "../loadingSquad";

let initPeople = {
    "name": <LoadingSquad width={150} height={20}/>,
    "height": <LoadingSquad width={80} height={30}/>,
    "mass": <LoadingSquad width={100} height={20}/>,
    "hair_color": <LoadingSquad width={100} height={25}/>,
    "skin_color": <LoadingSquad width={100} height={25}/>,
    "eye_color": <LoadingSquad width={100} height={25}/>,
    "homeworld": "http://swapi.dev/api/planets/1/",
    "spaceShips": [
        {name: <LoadingSquad width={120} height={25}/>, id: '#'}
    ],
};

let initPlanet = {
    "name": <LoadingSquad width={150} height={20}/>,
    "rotation_period": <LoadingSquad width={40} height={30}/>,
    "orbital_period": <LoadingSquad width={60} height={30}/>,
    "diameter": <LoadingSquad width={80} height={30}/>,
    "climate": <LoadingSquad width={60} height={30}/>,
    "gravity": "1 standard",
    "terrain": <LoadingSquad width={60} height={30}/>,
    "surface_water": "1",
    "population": <LoadingSquad width={100} height={30}/>,
    "residents": [
        {name: <LoadingSquad width={150} height={30}/>, id: '#'}
    ],
};

export default function PlanetInfo(props) {
    let [planet, setPlanet] = useState(initPlanet);

    let {match} = props;
    let {id} = match.params;

    let {rotation_period, orbital_period, diameter, population, terrain} = planet;

    useEffect(() => {
        setPlanet(initPlanet);
        planetsService.getOne(id).then((data) => {
            setPlanet(data);
        });
    }, [id])

    return (
        <Card title={planet.name} notFilter>
            <Label>
                Informações
            </Label>
            <Container>
                <InfoContainer>
                    <Info><Rotation/>{rotation_period instanceof Object ? rotation_period :parseInt(rotation_period).toLocaleString('pt-BR')} horas</Info>
                    <Info><Orbit/>{orbital_period instanceof Object ? orbital_period :parseInt(orbital_period).toLocaleString()} dias</Info>
                    <Info><Population/>{population instanceof Object ? population :parseInt(population).toLocaleString()} pessoas</Info>
                </InfoContainer>
                <BigContainer>
                    <Mass/>
                    <Height>{terrain}</Height>
                </BigContainer>
                <BigContainer>
                    <Planet/>
                    <PlanetTitle>{diameter ? diameter :parseInt(diameter).toLocaleString()} km</PlanetTitle>
                </BigContainer>
            </Container>
            {planet.residents.length > 0 && <>
                <Label>
                    Residentes
                </Label>
                <List>
                    {planet.residents.map(({name, id}, i) => <Item to={`/peoples/${id}`} key={i}>{name}</Item>)}
                </List>
            </>}
        </Card>
    );
}