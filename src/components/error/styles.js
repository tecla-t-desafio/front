import styled from "styled-components";

export const Container = styled.div`
    padding: 1em;
    text-transform: uppercase;
    font-weight: bold;
`;