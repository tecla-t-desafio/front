import React from "react";
import Card from "../card";
import {Container} from "./styles";

export default function Error(props){
    let {text} = props;
    return(
        <Card title={'error'} error notFilter>
            <Container>
                {text}
            </Container>
        </Card>
    )
}