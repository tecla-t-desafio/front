import styled, {keyframes} from "styled-components";

const backgroundAnimation = keyframes`
    0% {background-position: 0% 50%;}
    50% {background-position: 100% 50%;}
    100% {background-position: 0% 50%;}
`

export const Container = styled.div`
    width: ${props => props.width}px;
    height: ${props => props.height}px;
    animation: ${backgroundAnimation};
    border-radius: 5px;
    animation-timing-function: ease;
    animation-duration: 3s;
    animation-iteration-count: infinite;
    background: linear-gradient(to right, white, #DCE2F3 50%, white 100%);
    background-size: 400% 400%;
`;
