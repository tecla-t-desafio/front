import React from "react";
import {Container} from "./styles";

export default function LoadingSquad(props){
    return(
        <Container {...props}/>
    );
}
