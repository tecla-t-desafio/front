import * as React from "react";
import {List, Datagrid, TextField, Filter, SearchInput, CreateButton} from 'react-admin';
import Toolbar from '@material-ui/core/Toolbar';


const CustomFilter = (props) => (
    <Filter {...props}>
        <SearchInput placeholder="Localizar" source="q" alwaysOn/>
    </Filter>
);

const Actions = ({
                     basePath,
                     displayedFilters,
                     filters,
                     filterValues,
                     resource,
                     showFilter
                 }) => (
    <Toolbar>
        {filters && React.cloneElement(filters, {
            resource,
            showFilter,
            displayedFilters,
            filterValues,
            context: 'button',
        })}
        <CreateButton label="Criar" basePath={basePath}/>

    </Toolbar>
);


export default function UserList(props) {
    return (
        <List {...props} bulkActionButtons={false} filters={<CustomFilter/>} actions={<Actions/>}>
            <Datagrid rowClick={'edit'}>
                <TextField source="id"/>
                <TextField source="name"/>
                <TextField source="email"/>
            </Datagrid>
        </List>
    );
}