import UserList from './list';
import UserCreate from "./create";
import UserEdit from "./edit";

export default {
    list: UserList,
    create: UserCreate,
    edit: UserEdit
}