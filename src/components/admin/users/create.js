import React from "react";
import {Create, SimpleForm, TextInput, required} from 'react-admin';

export default function UserCreate(props){
    return (
        <Create title="Criar usuário" {...props}>
            <SimpleForm submitOnEnter={true} redirect="list">
                <TextInput validate={[required()]} label="Nome" source="name" />
                <TextInput validate={[required()]} label="Email" source="email" />
                <TextInput validate={[required()]} type={'password'} resettable label="Senha" source="password"/>
            </SimpleForm>
        </Create>
    );
}
