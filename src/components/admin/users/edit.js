import React from "react";
import {Edit, SimpleForm, TextInput, Toolbar, SaveButton, DeleteButton, required} from 'react-admin';
import { makeStyles } from '@material-ui/core/styles';

const UserName = ({record}) => {
    return <span>Editar usuário {record.name}</span>;
};

const useStyles = makeStyles({
    toolbar: {
        display: 'flex',
        justifyContent: 'space-between',
    },
});

const CustomToolbar = props => (
    <Toolbar {...props} classes={useStyles()}>
        <SaveButton />
        <DeleteButton resource="users" undoable={false} />
    </Toolbar>
);


export default function UserEdit(props){
    return (
        <Edit title={<UserName/>} {...props}>
            <SimpleForm toolbar={<CustomToolbar />} submitOnEnter={true} redirect="list">
                <TextInput validate={[required()]} label="Nome" source="name" />
                <TextInput validate={[required()]} label="Email" source="email" />
                <TextInput resettable label="Nova senha" source="password"/>
                <TextInput resettable label="Senha atual" source="oldPassword"/>
            </SimpleForm>
        </Edit>
    );
}
