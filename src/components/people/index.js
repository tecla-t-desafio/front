import React from "react";
import {Container, Gender, Man, Name, Woman} from "./styles";
import translate from "../../addons/translate";
import {Link} from "react-router-dom";

export default function People(props) {
    let {name, gender, selected, id} = props;

    let genderIcon = gender === 'male' ? <Man/> : <Woman/>;
    if (gender === 'n/a' || gender instanceof Object) genderIcon = '';

    return (
        <Link to={selected ? '/peoples' : `/peoples/${id}`}>
            <Container selected={selected}>
                <Name>{name}</Name>
                <Gender>{genderIcon}{gender instanceof Object ? gender : translate[gender]}</Gender>
            </Container>
        </Link>
    )
}