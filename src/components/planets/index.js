import React, {useEffect, useState} from "react";
import {Container, PlanetsContainer} from "./styles";
import Planet from "../planet";
import planetsService from '../../services/planets';
import LoadingSquad from "../loadingSquad";

let initPlanets = new Array(6).fill({
    "name": <LoadingSquad width={200} height={20}/>,
    "population": <LoadingSquad style={{marginTop: 5}} width={100} height={15}/>,
});

export default function Planets(props) {
    let [planets, setPlanets] = useState(initPlanets);

    let {match, page, setpagename, settotal, seterror} = props;
    let {id} = match.params;
    setpagename('Planetas');

    useEffect(() => {
        planetsService.getList(page, true).then(({data, total}) => {
            setPlanets(data);
            seterror(false);
            settotal(total);
        }).catch(() => seterror('Ocorreu um erro ao listar os planetas'));
    }, []);

    return (
        <Container>
            <PlanetsContainer>
                {planets.map((planet, i) => <Planet selected={parseInt(id) === planet.id} key={i} {...planet}/>)}
            </PlanetsContainer>
        </Container>
    );

}