import React from "react";
import {Button, Container, Logo, MenuButtons, People, Planet, SpaceShip, Text} from "./styles";

export default function Header(){
    return(
        <Container>
            <Logo/>
            <MenuButtons>
                <Button to={'/peoples'}><People/><Text>Pessoas</Text></Button>
                <Button to={'/spaceships'}><SpaceShip/><Text>Naves</Text></Button>
                <Button to={'/planets'}><Planet/><Text>Planetas</Text></Button>
            </MenuButtons>
        </Container>
    )
}