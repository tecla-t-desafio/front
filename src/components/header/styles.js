import styled from "styled-components";
import LogoImage from '../../assets/images/logo.png';
import React from "react";
import {BsPeopleFill} from "react-icons/bs";
import {FaSpaceShuttle} from "react-icons/fa";
import {IoMdPlanet} from "react-icons/io";
import {Link} from "react-router-dom";

export const Container = styled.div`
    display: flex;
    flex-direction: row;
    background: linear-gradient(to right, #00c6e9, #18db9f);
    padding: 0.6em 1.2em;
    border-radius: 12px;
    margin: 1em 0;
    box-shadow: 1px 1px 6px rgba(0,0,0,0.2);
    justify-content: space-between;
    
    @media (max-width: 768px) {
        padding: 0.6em;
    }
`

export const Logo = styled(props => <img {...props} src={LogoImage} alt={'StarWars'}/>)`
    height: 3em;
`;

export const Text = styled.p`
    @media (max-width: 768px) {
        display: none;
    }
`;

export const MenuButtons = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
`;

export const Button = styled(Link)`
    padding: 0.4em;
    cursor: pointer;
    font-weight: 600;
    text-transform: uppercase;
    font-size: 1.1em;
    color: white;
    margin: 0 1em;
    display: flex;
    flex-direction: row;
    align-items: center;
    transition: 0.2s all ease-in-out;
    
    @media (max-width: 768px) {
        margin: 0 0.2em;
    }
    
    &:hover{
        opacity: 0.8;
    }
`;

export const People = styled(BsPeopleFill)`
    margin: 0 0.4em;
    font-size: 1.4em;
`;

export const SpaceShip = styled(FaSpaceShuttle)`
    margin: 0 0.4em;
    font-size: 1.4em;
`;

export const Planet = styled(IoMdPlanet)`
    margin: 0 0.4em;
    font-size: 1.4em;
`;