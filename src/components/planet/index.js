import React from "react";
import {Container, Population, Name, Icon} from "./styles";
import {Link} from "react-router-dom";

export default function Planet(props) {
    let {name, population, selected, id} = props;

    return (
        <Link to={selected ? '/planets' : `/planets/${id}`}>
            <Container selected={selected}>
                <Name>{name}</Name>
                <Population><Icon/>{population instanceof Object ? population : parseInt(population).toLocaleString('pt-BR')}</Population>
            </Container>
        </Link>
    )
}