import styled from "styled-components";

import {IoIosPeople} from 'react-icons/io';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    padding: 1em;
    margin: 1em;
    color: ${props => props.selected ? 'white' : '#333333'};
    cursor: pointer;
    transition: 0.2s all ease;
    background: ${props => props.selected ? 'linear-gradient(to right,#00c6e9,#18db9f)' : 'transparent'};
    border-radius: 12px;

    &:hover{
        background-color: #f5f5f5;
    }
`;

export const Name = styled.h1`
    font-size: 1.2em;
    text-transform: capitalize;
    text-align: center;
`;

export const Population = styled.h2`
    font-size: 0.8em;
    opacity: 0.7;
    text-align: center;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    text-transform: uppercase;
`;

export const Icon = styled(IoIosPeople)`margin: 0 0.2em;`;