import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    padding: 1em;
`;

export const SpaceShipsContainer = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: left;
    flex-flow: row wrap;
    
    &::after {
      content: "";
      flex: auto;
    }
`;