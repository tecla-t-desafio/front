import React, {useEffect, useState} from "react";
import {Container, SpaceShipsContainer} from "./styles";
import SpaceShip from "../spaceShip";
import spaceShipsService from "../../services/spaceShips";
import LoadingSquad from "../loadingSquad";

let initSpaceShips = new Array(6).fill({
    "name": <LoadingSquad width={200} height={20}/>,
    "model": <LoadingSquad style={{marginTop: 5}} width={100} height={15}/>,
});

export default function SpaceShips(props){
    let [spaceShips, setSpaceShips] = useState(initSpaceShips);
    let {match, page, setpagename, settotal, seterror} = props;
    let {id} = match.params;
    setpagename('Naves espaciais');

    useEffect(() => {
        spaceShipsService.getList(page, true).then(({data, total}) => {
            setSpaceShips(data);
            settotal(total);
            seterror(false);
        }).catch(() => seterror('Ocorreu um erro ao listar as naves'));
    }, []);

    return(
        <Container>
            <SpaceShipsContainer>
                {spaceShips.map((spaceShip, i) => <SpaceShip selected={spaceShip.id === parseInt(id)} key={i} {...spaceShip}/>)}
            </SpaceShipsContainer>
        </Container>
    );

}