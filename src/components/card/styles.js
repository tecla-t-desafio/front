import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    background-color: white;
    box-shadow: 1px 1px 6px rgba(0,0,0,0.1);
    margin: 1em 0;
    margin-top: 3em;
    border-radius: 12px;
`;

export const TitleContent = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
`

export const Select = styled.select`
    font-size: 0.9em;
    margin: 0 0.6em;
    padding: 0.4em 1em;
    background-color: #f5f5f5;
    text-transform: uppercase;
    font-weight: bold;
`;