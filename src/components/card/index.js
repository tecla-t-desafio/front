import React from "react";
import {Container, Select, TitleContent} from "./styles";
import PageTitle from "../pageTitle";

export default function Card({children, notFilter, title, error}) {
    return (
        <Container>
            <TitleContent>
                <PageTitle title={title} error={error || (!(title instanceof Object) && title.toLowerCase()==='error')}/>
                {!notFilter &&
                <>
                    <Select>
                        <option value={'teste'}>Teste</option>
                    </Select>
                    <Select>
                        <option value={'teste'}>Teste</option>
                    </Select>
                </>}
            </TitleContent>
            {children}
        </Container>
    )
}