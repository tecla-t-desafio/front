import styled from "styled-components";
import {IoIosPeople} from 'react-icons/io';
import {GiCargoCrane} from 'react-icons/gi';
import {Link} from "react-router-dom";
import {MdTerrain} from 'react-icons/md';
import {FaSpaceShuttle} from 'react-icons/fa';

export const Container = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: left;
    flex-flow: row wrap;
    padding: 0 1em;
`;

export const InfoContainer = styled.div`
    display: flex;
    flex-direction: column;
    margin: 1em;
`;

export const Info = styled.p`
    font-size: 1.4em;
    display: flex;
    flex-direction: row;
    margin: 0.4em 0;
    align-items: center;
`;

export const BigContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 200px;
    height: 200px;
    align-items: center;
    justify-content: center;
    opacity: 0.8;
`;

export const Mass = styled(MdTerrain)`
    font-size: 100px;
`;

export const Height = styled.p`
    font-size: 1.2em;
    font-weight: bold;
    text-transform: capitalize;
`;

export const PlanetTitle = styled.p`
    font-size: 1.2em;
    font-weight: bold;
    text-align: center;
`;

export const List = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: left;
    flex-flow: row wrap;
    margin: 0.6em;
    
    &::after {
      content: "";
      flex: auto;
    }
`;

export const Label = styled.p`
    font-size: 1em;
    text-transform: uppercase;
    margin: 1em;
    opacity: 0.7;
    margin-bottom: 0;
`;

export const Item = styled(Link)`
    color: #333333;
    padding: 0.6em 1em;
    transition: 0.2s all ease;
    border-radius: 12px;
    font-size: 1.2em;
    margin: 0 0.2em;
    text-transform: capitalize;

    &:hover{
        background-color: #f5f5f5;
    }
`;


export const Cargo = styled(GiCargoCrane)`font-size: 100px`;
export const Passengers = styled(IoIosPeople)`margin-right: 0.4em;`;
export const SpaceShipIcon = styled(FaSpaceShuttle)`font-size: 100px`;