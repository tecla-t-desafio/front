import React, {useEffect, useState} from "react";
import Card from "../card";
import {
    Container,
    Height,
    BigContainer,
    Info,
    InfoContainer,
    SpaceShipIcon,
    PlanetTitle, List, Item, Label, Passengers, Cargo
} from "./styles";
import spaceShipsService from "../../services/spaceShips";
import LoadingSquad from "../loadingSquad";

let initSpaceShip = {
    "name": <LoadingSquad width={150} height={20}/>,
    "model": <LoadingSquad width={150} height={20}/>,
    "manufacturer": <LoadingSquad width={200} height={20}/>,
    "cost_in_credits": <LoadingSquad width={100} height={20}/>,
    "length": <LoadingSquad width={150} height={20}/>,
    "passengers": <LoadingSquad width={150} height={20}/>,
    "cargo_capacity": <LoadingSquad width={150} height={20}/>,
    "consumables": "1 year",
    "hyperdrive_rating": "2.0",
    "MGLT": "60",
    "starship_class": "corvette",
    "pilots": [],
    "films": [
        "http://swapi.dev/api/films/1/",
        "http://swapi.dev/api/films/3/",
        "http://swapi.dev/api/films/6/"
    ],
    "created": "2014-12-10T14:20:33.369000Z",
    "edited": "2014-12-20T21:23:49.867000Z",
    "url": "http://swapi.dev/api/starships/2/"
};

export default function SpaceShipInfo(props) {
    let [spaceShip, setSpaceShip] = useState(initSpaceShip);
    let {match} = props;
    let {id} = match.params;

    let {model, manufacturer, length, passengers, cargo_capacity} = spaceShip;

    useEffect(() => {
        setSpaceShip(initSpaceShip);
        spaceShipsService.getOne(id, true).then((data) => {
            setSpaceShip(data);
        });
    }, [id])

    return (
        <Card title={spaceShip.name} notFilter>
            <Label>
                Informações
            </Label>
            <Container>
                <InfoContainer>
                    <Info><Passengers/>{passengers}</Info>
                    <Info>Modelo: {model}</Info>
                    <Info>Fabricante: {manufacturer}</Info>
                </InfoContainer>
                <BigContainer>
                    <Cargo/>
                    <Height>{cargo_capacity instanceof Object ? cargo_capacity :parseInt(cargo_capacity).toLocaleString()} kg</Height>
                </BigContainer>
                <BigContainer>
                    <SpaceShipIcon/>
                    <PlanetTitle>{length instanceof Object ? length : parseInt(length).toLocaleString()} m</PlanetTitle>
                </BigContainer>
            </Container>
            {spaceShip.pilots.length > 0 &&
            <>
                <Label>
                    Pilotos
                </Label>
                <List>
                    {spaceShip.pilots.map(({name, id}, i) => <Item to={`/peoples/${id}`} key={i}>{name}</Item>)}
                </List>
            </>}
        </Card>
    );
}