import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    padding: 1em;
    
`;

export const PeoplesContainer = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: left;
    flex-flow: row wrap;
    
    &::after {
      content: "";
      flex: auto;
    }
    
    @media (max-width: 768px) {
        justify-content: center;
        &::after {
            flex: initial;
        }
    }
`;