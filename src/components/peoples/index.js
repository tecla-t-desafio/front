import React, {useEffect, useState} from "react";
import {Container, PeoplesContainer} from "./styles";
import People from "../people";
import peoplesService from '../../services/peoples';
import LoadingSquad from "../loadingSquad";

let initPeoples = new Array(6).fill({
    "name": <LoadingSquad width={200} height={20}/>,
    "gender": <LoadingSquad style={{marginTop: 5}} width={100} height={15}/>,
});

export default function Peoples(props) {
    let [peoples, setPeoples] = useState(initPeoples);
    let {match, page, setpagename, settotal, seterror} = props;
    let {id} = match.params;
    setpagename('Pessoas');

    useEffect(() => {
        peoplesService.getList(page).then(({data, total}) => {
            seterror(false);
            setPeoples(data);
            settotal(total);
        }).catch(() => seterror('Ocorreu um erro ao listar as pessoas'));
    }, []);

    return (
        <Container>
            <PeoplesContainer>
                {peoples.map((people, i) => <People selected={people.id === parseInt(id)} key={i} {...people}/>)}
            </PeoplesContainer>
        </Container>
    );

}