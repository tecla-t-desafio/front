import React from "react";
import {Container, Left, Number, Right} from "./styles";

export default function Pagination(props){
    let {page, total, perpage, setpage} = props;
    let maxPage = parseInt(total/perpage) + (total%10? 1 : 0);

    let next = () => {
        if(page >= maxPage) return;
        setpage(page+1);
    }

    let previous = () => {
        if(page <= 1) return;
        setpage(page-1);
    }

    return(
        <Container>
            <Left onClick={previous} page={page}/>
            <Number>{page}</Number>
            <Right onClick={next} page={page} maxpage={maxPage}/>
        </Container>
    )

}