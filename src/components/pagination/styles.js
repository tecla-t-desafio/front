import styled from "styled-components";
import {IoIosArrowBack, IoIosArrowForward} from 'react-icons/io';

export const Container = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    color: #33333;
    justify-content: flex-end;
    margin: 1em;
    margin-top: 0;
    opacity: 0.8;
    user-select: none;
`;

export const Left = styled(IoIosArrowBack)`
    font-size: 1.4em;
    margin: 0 0.4em;
    cursor: pointer;
    opacity: ${props => props.page <= 1 ? 0 : 1};
`;

export const Right = styled(IoIosArrowForward)`
    font-size: 1.4em;
    margin: 0 0.4em;
    cursor: pointer;
    opacity: ${props => props.page >= props.maxpage ? 0 : 1};
`;

export const Number = styled.p`
    font-size: 1.2em;
    width: 15px;
    text-align: center;
`;