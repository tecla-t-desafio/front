
const dataProvider = (host) => ({
    getList: async (resource, {pagination, sort, filter}) => await sendRequest(host, resource, 'GET', {...pagination, ...sort, ...filter}),
    getOne: async (resource, {id}) => await sendRequest(host, resource, 'GET', {}, null, id),
    create: async (resource, {data}) => await sendRequest(host, resource, 'POST', {}, {...data}),
    update: async (resource, {id}) => await sendRequest(host, resource, 'PUT', {}, null, id),
    delete: async (resource, {id}) => await sendRequest(host, resource, 'DELETE', {}, null, id),
});

async function sendRequest(host, route, method, queryString = {}, body, element = '') {
    let token = localStorage.getItem('token');

    let queryStringObject = queryString;
    queryString = [];
    for (let key in queryStringObject) queryString.push(`${key}=${queryStringObject[key]}`);
    queryString = queryString.join('&');

    let requestOptions = {
        headers: {
            authorization: `Bearer ${token}`,
            'content-type': 'application/json'
        },
        method,
        ...(body ? {body: JSON.stringify(body)} : {})
    };

    let response = await fetch(`${host}/${route}/${element}${queryString ? `?${queryString}` : ''}`, requestOptions);
    let json = await response.json();
    if (response.status < 200 || response.status >= 300) return Promise.reject(json.message);

    return json;
}


export default dataProvider;