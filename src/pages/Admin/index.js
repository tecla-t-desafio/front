import React from "react";
import { Admin, Resource } from 'react-admin'
import dataProvider from "../../dataProvider";
import authProvider from "../../services/authProvider";
import users from '../../components/admin/users/index';

export default function AdminPage(){
    let host = '/api/v1';

    return (
        <Admin title={'Tecla T - Desafio'} authProvider={authProvider} dataProvider={dataProvider(host)}>
            <Resource name="users" options={{ label: 'Usuários' }} {...users} />
        </Admin>
    );
}