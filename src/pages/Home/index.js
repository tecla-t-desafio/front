import React, {useState} from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Peoples from "../../components/peoples";
import {Container} from "./styles";
import Header from "../../components/header";
import Card from "../../components/card";
import Pagination from "../../components/pagination";
import PeopleInfo from "../../components/peopleInfo";
import Planets from "../../components/planets";
import PlanetInfo from "../../components/planetInfo";
import SpaceShips from "../../components/spaceShips";
import SpaceShipInfo from "../../components/spaceShipInfo";
import Error from "../../components/error";

export default function Home(props) {
    let [page, setPage] = useState(1);
    let [pageName, setPageName] = useState('');
    let [total, setTotal] = useState(10);
    let [error, setError] = useState(false);
    let perPage = 10;

    return (
        <BrowserRouter>
            <Container>
                <Header/>
                {error && <Error text={error}/>}
                <Card title={pageName} notFilter>
                    <Switch>
                        <Route path={['/peoples', '/peoples/:id', ['/']]} exact
                               component={(props) => <Peoples {...props} seterror={setError} settotal={setTotal} setpagename={setPageName} page={page} perpage={perPage}/>}/>
                        <Route path={['/planets', '/planets/:id']} exact
                               component={(props) => <Planets {...props} seterror={setError} settotal={setTotal} setpagename={setPageName} page={page} perpage={perPage}/>}/>
                        <Route path={['/spaceships', '/spaceships/:id']} exact
                               component={(props) => <SpaceShips {...props} seterror={setError} settotal={setTotal} setpagename={setPageName} page={page} perpage={perPage}/>}/>
                    </Switch>
                    <Pagination page={page} setpage={setPage} perpage={perPage} total={total}/>
                </Card>
                <Switch>
                    <Route path="/peoples/:id" exact component={(props) => <PeopleInfo seterror={setError} {...props}/>}/>
                    <Route path="/planets/:id" exact component={PlanetInfo}/>
                    <Route path="/spaceships/:id" exact component={SpaceShipInfo}/>
                </Switch>
            </Container>
        </BrowserRouter>

    );
}