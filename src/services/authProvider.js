import fetch from 'node-fetch';
let host = 'http://localhost:3000/api/v1';

export default {
    login: ({username, password}) => {

        return new Promise(async (resolve, reject) => {
            let response = await fetch(host + '/auth', {
                method: 'POST',
                headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
                body: JSON.stringify({
                    email: username,
                    password: password
                })
            });

            let json = await response.json();

            if(response.status !== 200) return reject(json.message);

            let {data} = json;
            localStorage.setItem('token', data.token);
            resolve();
        });
    },
    logout: () => {
        localStorage.removeItem('token');
        return Promise.resolve();
    },
    checkError: error => {
        console.log(error);
    },
    checkAuth: () => {
        return localStorage.getItem('token') ? Promise.resolve() : Promise.reject();
    },
    getPermissions: () => Promise.resolve(),
};
