import dataProvider from "../dataProvider";
import peoplesService from "./peoples";

const url ='https://swapi.dev/api';

export default {
    async getList(page){
        let {count: total, results: data} = await dataProvider(url).getList('planets', {pagination: {page}});
        for(let element of data) element.id = parseInt(element.url.split('/')[element.url.split('/').length - 2]);
        data = data.filter(item => item.id);
        return {total, data};
    },
    async getOne(id, getResidents = false){
        let response = await fetch(`${url}/planets/${id}/`);
        let json = await response.json();
        if (response.status < 200 || response.status >= 300) return Promise.reject(json.message);

        if(getResidents) {
            let {residents} = json;
            json.residents = [];

            for (let id of residents.map(resident => resident.split('/')[resident.split('/').length - 2])) {
                let {name} = await peoplesService.getOne(id);
                json.residents.push({name, id});
            }
        }

        return json;
    }
}