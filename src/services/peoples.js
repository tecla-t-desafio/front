import dataProvider from "../dataProvider";
import spaceShipsService from "./spaceShips";

const url ='https://swapi.dev/api';

export default {
    async getList(page){
        let {count: total, results: data} = await dataProvider(url).getList('people', {pagination: {page}});
        for(let element of data) element.id = parseInt(element.url.split('/')[element.url.split('/').length - 2]);
        data = data.filter(item => item.id);
        return {total, data};
    },
    async getOne(id, getSpaceShips){
        //Necessario pois API estava com erro de cors quando passava uma '/' no final do url
        let response = await fetch(`${url}/people/${id}`);
        let json = await response.json();
        if (response.status < 200 || response.status >= 300) return Promise.reject(json.message);

        if(getSpaceShips) {
            let {starships: spaceShips} = json;
            json.spaceShips = [];

            for (let id of spaceShips.map(spaceShip => spaceShip.split('/')[spaceShip.split('/').length - 2])) {
                let {name} = await spaceShipsService.getOne(id);
                json.spaceShips.push({name, id});
            }
        }

        return json;
    }
}