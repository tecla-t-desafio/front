import dataProvider from "../dataProvider";
import peoplesService from "./peoples";

const url ='https://swapi.dev/api';

export default {
    async getList(page){
        let {count: total, results: data} = await dataProvider(url).getList('starships', {pagination: {page}});

        for(let element of data) element.id = parseInt(element.url.split('/')[element.url.split('/').length - 2]);
        data = data.filter(item => item.id);

        return {total, data};
    },
    async getOne(id ,getPilots = false){
        let response = await fetch(`${url}/starships/${id}`);
        let json = await response.json();
        if (response.status < 200 || response.status >= 300) return Promise.reject(json.message);

        if(getPilots) {
            let {pilots} = json;
            json.pilots = [];

            for (let id of pilots.map(pilot => pilot.split('/')[pilot.split('/').length - 2])) {
                let {name} = await peoplesService.getOne(id);
                json.pilots.push({name, id});
            }
        }

        return json;
    }
}